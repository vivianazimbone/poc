var CameraView = function () {
    var cv = this;

    this.initialize = function () {
        this.validateUser();
        this.$el = $('<div/>');
        this.$el.on('click', '.setPicButton', this.setPicture);
        this.$el.on('click', '.savePicButton', this.savePicture);
        this.$el.on('click', '.storePicButton', this.storePicture);
        this.render();
    };

    /**
     * show div overlay for modal control
     */
    function showOverlay() {
        document.getElementById("overlay").hidden = false;
    };

    /**
     * hide div overlay for modal control
     */
    function hideOverlay() {
        document.getElementById("overlay").hidden = true;
    };

    /**
     * Get user data
     */
    this.validateUser = function() {
     var user = localStorage.getItem('user');
     if (user) {
        $.get({
            dataType: "json",
            data: {'user': user},
            url: localStorage.getItem('jsonUrl') + "users",
            success: this.onSuccess,
            error: this.onError
        });
     }
    };

    /**
     *
     * @param data, the user data
     */
    this.onSuccess = function (data) {
        console.log('Data: ' + data);
        if (data != null) {
            // User found
            if (data.photo && data.photo.length > 0) {
                getFromDropbox(data.photo);
            }
        } else {
            // New user
            addUser(localStorage.getItem('user'));
        }
    };

    this.onError = function(data, textStatus, errorThrown) {
        console.log('Data: ' + data);
        console.log('Status: ' + textStatus);
        console.log('Error: ' + errorThrown);
        console.log('Exiting onError');
    };

    this.render = function () {
         this.$el.html(this.template());
         return this;
    };

    /**
     * Save a new user
     * @param user, the new user
     */
    function addUser(user) {
    console.log("addUser");
        $.post({
            data: JSON.stringify({'user': user, 'photo' : ''}),
            url: localStorage.getItem('jsonUrl') + "users",
            success: function(data) {
                             console.log("New user added");
                         },
            error: this.onError
        });
    };

    /**
     * Get user photo stored in application dropbox folder
     * by means of dropbox api v2
     * @param id
     */
    function getFromDropbox(id) {

        showOverlay();
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = function() {
            hideOverlay();
            if (xhr.status === 200) {
                    onDropboxResponse(xhr.response);
            }
            else {
                var errorMessage = xhr.response || 'Unable to download file';
                console.log(errorMessage);
            }
        };
        xhr.onerror = function () {
            hideOverlay();
        };

        xhr.open('GET', 'https://content.dropboxapi.com/2/files/download');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('dropboxToken'));
        xhr.setRequestHeader('Dropbox-API-Arg', JSON.stringify({
            path: id,
        }));

        xhr.send();
    };

    /**
     * Convert the stream to show picture
     * @param data
     */
    onDropboxResponse = function(data) {
        console.log('onDropboxResponse');
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.userImage', cv.$el).attr('src', e.target.result);
        };
        reader.readAsDataURL(new Blob([data]));
    };

    /**
     * Get a picture through camera api
     * @param event
     * @returns {boolean}
     */
    this.setPicture = function(event) {
      event.preventDefault();
      if (!navigator.camera) {
          alert("Camera API not supported", "Error");
          return;
      }
      var options =   {   quality: 20,
                          destinationType: Camera.DestinationType.DATA_URL,
                          sourceType: Camera.PictureSourceType.CAMERA,
                          encodingType: Camera.EncodingType.JPEG,
                          correctOrientation: true
                      };
      if (device.platform == 'browser') {
        var body = document.getElementById('body');
        body.classList.add('static');
      }
      navigator.camera.getPicture(
          function(imgData) {
            document.getElementById('savePicButton').disabled = false;
            document.getElementById('storePicButton').disabled = false;
            $('.userImage', this.$el).attr('src', "data:image/jpeg;base64,"+imgData);
          },
          function() {
              alert('Error taking picture', 'Error');
          },
          options);

      return false;
    };

    /**
     * Store picture on disk
     */
    this.savePicture = function() {
        var imageSource = $('.userImage').attr('src');
        if ( device.platform == 'Android' || device.platform == 'iOS' || device.platform == 'WWinCE') {
            var successCallback = function(filePath) {
                console.log('Picture saved on ' + filePath);
                window.alert('Picture saved on ' + filePath.replace('file:///', ''), 'Image Saved', 'CLOSE');
            };
            var errorCallback = function (msg) {
              console.error(msg);
            };
            window.imageSaver.saveBase64Image({data:imageSource, prefix: 'poc_', mediaScanner: true},
                successCallback, errorCallback);
        } else if ( device.platform == 'browser') {
                var link = document.createElement("a");
                link.setAttribute("href", imageSource);
                link.setAttribute("download", 'poc_' + Date.now());
                link.click();
            } else {
            console.log('Saving images is not supported');
            window.alert('Saving images is not supported');
        }
    };

    /**
     * Convert a base 64 image to an Array buffer
     * @param base64
     * @returns {ArrayBuffer}
     */
    function base64ToArrayBuffer(base64) {
        base64 = base64.split('data:image/jpeg;base64,').join('');
        var binary_string =  window.atob(base64),
            len = binary_string.length,
            bytes = new Uint8Array( len ),
            i;

        for (i = 0; i < len; i++)        {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    };

    /**
     * Update pic info for user
     * @param id
     * @param user
     */
    function updatePicInfo (id, user) {
        console.log("updatePicInfo");
        $.post({
            data: JSON.stringify({'user' : user, 'photo' : id}),
            contentType: 'application/json',
            url: localStorage.getItem('jsonUrl') + "users",
            success: function(data) {
                console.log("Pic info added");
            },
            error: this.onError
        });
    };

    /**
     * Change progress bar percent
     * @param percent
     */
    function move(percent) {
        var elem = document.getElementById("myBar");
            elem.style.width = percent + '%';
            document.getElementById("label").innerHTML = percent + '%';
    };

    /**
     * Store picture on application dropbox folder
     * by means of drobpox api v2
     */
    this.storePicture = function() {
        showOverlay();
        var base64 = $('.userImage').attr('src');
        var byteArray = base64ToArrayBuffer(base64);
        var user = localStorage.getItem('user');
        var xhr = new XMLHttpRequest();

        xhr.upload.onprogress = function(evt) {
            var percentComplete = parseInt(100.0 * evt.loaded / evt.total);
            // Upload in progress
                move(percentComplete);
        };

        xhr.onload = function() {
                hideOverlay();
            if (xhr.status === 200) {
                var fileInfo = JSON.parse(xhr.response);
                // Upload succeeded
                document.getElementById("myProgress").hidden = true;
                console.log("Pic Stored");
                updatePicInfo(fileInfo.id, user);
            }
            else {
                var errorMessage = xhr.response || 'Unable to upload file';
                document.getElementById("myProgress").hidden = true;
                console.log(errorMessage);
            }
        };
        xhr.onerror = function () {
            hideOverlay();
        };

        xhr.open('POST', 'https://content.dropboxapi.com/2/files/upload');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('dropboxToken'));
        xhr.setRequestHeader('Content-Type', 'application/octet-stream');
        xhr.setRequestHeader('Dropbox-API-Arg', JSON.stringify({
            path: '/' +  user + '.jpg',
            mode: 'overwrite',
        }));

        xhr.send(byteArray);
        document.getElementById("myProgress").hidden = false;
    }
    this.initialize();
};