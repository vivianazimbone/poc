var HomeView = function () {

    this.initialize = function () {
        this.$el = $('<div/>');
        this.render();
    };

    this.render = function () {
        var context = {title: 'Welcome to poc-Cordova'};
         this.$el.html(this.template(context));
         return this;
    };
    this.initialize();
};