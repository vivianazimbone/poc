// We use an "Immediate Function" to initialize the application to avoid leaving anything behind in the global scope
(function () {

    /* ---------------------------------- Local Variables ---------------------------------- */
    HomeView.prototype.template = Handlebars.compile($("#homeTemplate").html());
    LoginView.prototype.template = Handlebars.compile($("#loginTemplate").html());
    CameraView.prototype.template = Handlebars.compile($("#cameraTemplate").html());
    localStorage.setItem('jsonUrl', 'http://raspyhome.ddnsking.com:58080/poc-rest/rs/');
    localStorage.setItem('dropboxToken', 'Ek6yCB6fWd4AAAAAAAAktiYqwfFHrxLEihP0ftf8sIb8Hh1aQitbj29R5MIJW6p9');
    /* --------------------------------- Event Registration -------------------------------- */
    document.addEventListener('deviceready', function () {
        if (device.platform != 'browser' && navigator.notification) {
            // Override default HTML alert with native dialog
            window.alert = function (message, title, buttonName) {
                navigator.notification.alert(
                message,    // message
                null,   // callback
                title,  // title
                buttonName  // buttonName
                );
            };
        };

        FastClick.attach(document.body);

        router.addRoute('', function() {
            $('body').html(new HomeView().render().$el);
        });

        router.addRoute('login', function() {
                    $('body').html(new LoginView().render().$el);
                });

        router.addRoute('camera', function() {
            $('body').html(new CameraView().render().$el);
        });

        router.start();

        welcome();

    }, false);

    /* ---------------------------------- Local Functions ---------------------------------- */
    function welcome() {
        console.log('welcome');
        alert('You are on ' + device.platform, "Welcome", 'CLOSE');
    };

    login = function() {
        var user = $('.userInput', this.$el).val().trim().toLowerCase();
        console.log("user: " + user);
        localStorage.setItem("user", user);
        router.load('camera');
    };


}());